import React,{useState, useEffect} from 'react'
import { useDispatch } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { createUserStart,updateUserStart } from '../redux/actions';

const initialState = {
    name : " ",
    email : " ",
    phone : " ",
    address : " ",
    };
    
const AddUser = () => {

    
    const [formValue, setFormValue] = useState(initialState);
    const[editMode, seteditMode] = useState(false);
    const {name,email,phone, address} = formValue;
    const dispatch = useDispatch();
    const history = useHistory();
    const {id} = useParams();
    const {users} = useSelector((state) => state.data )
   
    useEffect(() => {
      
      if(id){
        seteditMode(true);
        const singleuser = users.find((user) => user.id === Number(id));
        setFormValue({...singleuser});
      }
      
    }, [id])

    const handleSubmit = (e) => {
      e.preventDefault();
      if (name && email && phone && address){
        if(!editMode){
          dispatch(createUserStart(formValue));
          
          setTimeout(() => history.push("/"), 500);
        } else{
          dispatch(updateUserStart({id, formValue}));
          seteditMode(false);
          window.alert("User Detals Updated");
         setTimeout(() => history.push("/"), 500);
         //history.push("/")
        }
        
      }
    }; 

    const onInputchange = (e) => {
        let {name, value} = e.target;
        setFormValue({...formValue, [name]: value })
    };
        
    return (
        <div className="container">
      <div className="w-75 mx-auto shadow p-5 ">
        <h2 className="text-center mb-4">{!editMode ? "Add User Details" : "Update User Details"}</h2>

        <form onSubmit={handleSubmit}>
  <div class="form-group">
    <label for="exampleInputEmail1">User Name</label>
    <input type="text" class="form-control" name="name" onChange={onInputchange} value={name || ""}  required />
    <br/>
    <label for="exampleInputEmail1">Email address</label>
    <input type="email" class="form-control" name="email" onChange={onInputchange} value={email || ""} required />
    <br/>
    <label for="exampleInputEmail1">Phone NO</label>
    <input type="text" class="form-control" name="phone" onChange={onInputchange} value={phone || ""}  required />
    <br/>
    <label for="exampleInputEmail1">Address</label>
    <input type="text" class="form-control" name="address" onChange={onInputchange} value={address || ""}  required />
  </div>
 
  <button type="submit" class={!editMode ? "btn btn-primary" :"btn btn-warning"}>{!editMode ? "Add User" :"Update User"}</button>
</form>

</div>
</div>
    )
}

export default AddUser
