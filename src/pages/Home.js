// import '../sass/mystyle.scss'
import React, {useEffect} from 'react'
import { useDispatch,useSelector } from 'react-redux'
import { deleteUserStart, loadUsersStart } from '../redux/actions';
import { toast } from 'react-toastify';
import { useHistory,Link } from 'react-router-dom';


//import { MDBTable, MDBTableHead, MDBTableBody, MDBBtn, MDBSpinner,MDBIcon,MDBTooltip } from 'mdb-react-ui-kit';


const Home = () => {
    const dispatch = useDispatch();
    const {users} = useSelector(state => state.data);
    const history = useHistory();

    

    useEffect(() => { 
        dispatch(loadUsersStart())
    },[])

    const handleDelete = (id) =>{
      if (window.confirm("Are sure..?")){
        dispatch(deleteUserStart(id));
        toast.success("User Deleted...");
      }
    }

    return (
      <div className="container">
      <div className="w-100 mx-auto shadow p-5 ">
          <h2>USERS LIST</h2>
          <table class="table table-light">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Name</th>
      <th scope="col">Email</th>
      <th scope="col">Phone</th>
      <th scope="col">Address</th>
      <th scope="col" >Action</th>
    </tr>
  </thead>
  {users && users.map((user, index) =>(
  <tbody>
    <tr>
      <th  scope="row">{index + 1}</th>
      <td className="td">{user.name}</td>
      <td className="td">{user.email}</td>
      <td className="td">{user.phone}</td>
      <td className="td">{user.address}</td>
      <td className="td"> 
      <Link to={`/edituser/${user.id}`} >
      <button  class="btn btn-warning">Edit</button>
      </Link>
      
      &nbsp;&nbsp;
      <button  class="btn btn-danger" onClick={() => handleDelete(user.id)}>Delete</button>
      </td>
    </tr>
  </tbody>
  ))}
  
</table>
        </div>
        </div>
    )
}

export default Home
