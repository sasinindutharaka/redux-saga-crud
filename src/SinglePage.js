import Home from "./pages/Home";
import AddUser from "./pages/AddUser";

import React from 'react'

const SinglePage = () => {
    return (
        <div>
           <Home />
           <AddUser /> 
        </div>
    )
}

export default SinglePage
