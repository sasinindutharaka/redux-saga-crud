import './App.css';
import Home from './pages/Home';
import Headerbar from './component/Headerbar';
import AddUser from './pages/AddUser';
import { BrowserRouter,Switch,Route } from 'react-router-dom';
import SinglePage from './SinglePage';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Headerbar />

    <Switch>
    <Route exact path="/" component={Home} />
      <Route  path="/addUsers" component={AddUser} />
      <Route  path="/edituser/:id" component={AddUser} />
      <Route  path="/singlepage" component={SinglePage} />
      
    </Switch>
    </BrowserRouter>
    </div>
  );
}

export default App;
