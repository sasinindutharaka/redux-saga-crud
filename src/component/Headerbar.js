import React from 'react';
import { Link } from 'react-router-dom';

function Headerbar() {
    return (
        <nav className="navbar navbar-expand-md navbar-dark bg-dark shadow-sm">
            
                <span className="navbar-brand mb-0 p-1 h1">USER LIST APPLICATION USING REDUX SAGA</span>
                <ul  className="navbar-nav">
                    <nav-link className="nav-item">
                        <Link className="nav-link" to="/">Home</Link>
                    </nav-link>


                    <nav-link className="nav-item">
                        <Link className="nav-link" to="/addusers">Add Users</Link>
                        </nav-link>
                    
                   
                    {/* <nav-link className="nav-item">
                        <Link className="nav-link" to="/singlepage">Table View</Link>
                    </nav-link> */}


                </ul>
                

            
        </nav>
    );
}
export default Headerbar;