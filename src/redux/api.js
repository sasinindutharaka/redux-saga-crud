import axios from "axios";

export const loadUsersApi = async () =>
    await axios.get("http://localhost:5000/users");

export const createUserApi = async (user) =>
    await axios.post("http://localhost:5000/users", user);

export const deleteUserApi = async (userid) =>
    await axios.delete(`http://localhost:5000/users/${userid}`);

export const updateUserApi = async (userid, userInfo) =>
    await axios.put(`http://localhost:5000/users/${userid}`, userInfo);
